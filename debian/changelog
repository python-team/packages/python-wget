python-wget (3.2-5) UNRELEASED; urgency=medium

  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Tue, 08 Nov 2022 17:52:22 -0000

python-wget (3.2-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 16:03:04 -0400

python-wget (3.2-3) unstable; urgency=medium

  * Team upload.
  * Rename d/tests/control.autodep8 to d/tests/control.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump standards version to 4.4.0 (no changes).
  * Bump debhelper compat level to 12.

 -- Ondřej Nový <onovy@debian.org>  Thu, 01 Aug 2019 12:27:34 +0200

python-wget (3.2-2) unstable; urgency=medium

  * Team upload.
  * Fixed homepage (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Enable autopkgtest-pkg-python testsuite
  * Drop python-urllib3 from runtime depends (Closes: #914506)
  * Add download autopkgtest
  * Bump standards version to 4.2.1 (no changes)
  * Bump debhelper compat level to 11

 -- Ondřej Nový <onovy@debian.org>  Sat, 24 Nov 2018 17:50:36 +0100

python-wget (3.2-1) unstable; urgency=medium

  * Upstream update

 -- Balasankar C <balasankarc@autistici.org>  Sun, 08 Nov 2015 10:47:46 +0530

python-wget (2.2-1) unstable; urgency=low

  * Initial upload (Closes: #781572)

 -- Balasankar C <balasankarc@autistici.org>  Mon, 30 Mar 2015 01:09:57 +0530
